﻿using System.Text.Json.Serialization;

namespace SeanBurns.Website.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string Maker { get; set; }
        public string Url { get; set; }
        [JsonPropertyName("img")]
        public string Image { get; set; }
        public string Title { get; set; }
        /*public override string ToString()
        {
            Json
        }*/
    }
}
